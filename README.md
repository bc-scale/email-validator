# Email Validator

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![Docker Stars][docker-stars-badge]][docker-repository]
[![Docker Pulls][docker-pulls-badge]][docker-repository]

[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]
[![Quality gate][sonar-quality-gate-badge]][sonar-website]
[![Code coverage][sonar-coverage-badge]][sonar-website]
[![Renovate enabled][renovate-badge]][renovate-website]

Web service which exposes endpoints to validate email addresses.

Email Validator is an HTTP wrapper, built with [`Starlite`][github-starlite],
for the following Python libraries:

- [`py3-validate-email`][gitea-py3-validate-email], which implements the core email validation logic.
- [`pymailcheck`][github-pymailcheck], used to provide suggestions to misspelled email addresses.

HTTP API has been modeled after those exposed by [ZeroBounce][zerobounce-api] and [SendGrid][sendgrid-api].

Please remember that proper email validation **always** requires sending an actual email
with a callback link to the address being validated, while this web service *only* verifies
that all elements required for an email to be successfully sent to the address being validated
are available and are working as expected.

This web service should be used in a scenario where a user can be helped
by knowing immediately whether an email address seems to be valid or not:
for example, if an administrator manually registers a user on a platform,
it might use the information that the email address seems to be invalid
to double check input data before sending the registration email to the final user.

:warning: Please carefully check the [SMTP validation](#smtp-validation) section
and [`py3-validate-email` FAQs][gitea-py3-validate-email-faq] in order to prevent
common validation issues.

## Table of Contents

- [Install](#install)
  - [Tags](#tags)
  - [Configuration](#configuration)
    - [`SECURITY_API_KEYS`](#security_api_keys)
    - [`VALIDATOR_CHECK_FORMAT`](#validator_check_format)
    - [`VALIDATOR_CHECK_BLACKLIST`](#validator_check_blacklist)
    - [`VALIDATOR_CHECK_DNS`](#validator_check_dns)
    - [`VALIDATOR_DNS_TIMEOUT`](#validator_dns_timeout)
    - [`VALIDATOR_CHECK_SMTP`](#validator_check_smtp)
    - [`VALIDATOR_SMTP_TIMEOUT`](#validator_smtp_timeout)
    - [`VALIDATOR_SMTP_HELO_HOST`](#validator_smtp_helo_host)
    - [`VALIDATOR_SMTP_FROM_ADDRESS`](#validator_smtp_from_address)
    - [`VALIDATOR_SMTP_SKIP_TLS`](#validator_smtp_skip_tls)
    - [`VALIDATOR_SMTP_DEBUG`](#validator_smtp_debug)
    - [`VALIDATOR_ADDRESS_TYPES`](#validator_address_types)
    - [`HEALTH_CHECK_PING_WEBSITES`](#health_check_ping_websites)
    - [`HEALTH_CHECK_PING_TIMEOUT`](#health_check_ping_timeout)
    - [`APPINSIGHTS_CONNECTION_STRING`](#appinsights_connection_string)
    - [`ZEROBOUNCE_API_KEY`](#zerobounce_api_key)
    - [`ZEROBOUNCE_CREDITS_THRESHOLD`](#zerobounce_credits_threshold)
  - [Logging](#logging)
  - [SMTP validation](#smtp-validation)
- [Usage](#usage)
  - [Verdicts](#verdicts)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
  - [Editing](#editing)
  - [Restoring dependencies](#restoring-dependencies)
  - [Starting development server](#starting-development-server)
  - [Running tests](#running-tests)
  - [Building Docker image](#building-docker-image)
- [License](#license)

## Install

Email Validator web service is provided as a Docker image hosted on [Docker Hub][docker-repository].

You can quickly start a local test instance with following command:

```bash
docker run -it --rm -p 8080:8080 pommalabs/email-validator:latest
```

Local test instance will be listening on port 8080 and it will accept **unauthenticated** requests.
Please check the [Configuration](#configuration) section to find further information
about how the web service can be properly configured.

Email Validator requires an internet connection in order to validate email addresses.
Please refer to [`py3-validate-email`][gitea-py3-validate-email] documentation
in order to find out which checks are performed and why an internet connection is needed.

### Tags

Following tags are available:

- `latest`, based on `python:3.11-slim-bullseye`, contains the code found in `main` branch.
- `preview`, based on `python:3.11-slim-bullseye`, contains the code found in `preview` branch.

Each tag is rebuilt at least every week.

### Configuration

Docker image can be configured using the following environment variables.
Moreover, the same variables can also be set by mounting a `.env` file in `/opt/app/`.

#### `SECURITY_API_KEYS`

Optional API keys which should be specified in order to enforce authentication
on incoming HTTP requests. API keys might not specified for local setups, but it is
strongly advised to specify it if Email Validator web service is exposed to the internet.

Each API key is described by three properties:

- A descriptive name (`name` field).
- The key value (`value` field).
- An optional expiration timestamp (`expiresAt` field).

If at least one API key is specified, then HTTP calls
need to include it with the `X-Api-Key` header.

Examples:

```bash
# No API key is defined (default).
SECURITY_API_KEYS='[]'

# Define one API key which never expires.
SECURITY_API_KEYS='[{"name":"My API key","value":"mySecret!"}]'

# Define two API keys which never expire.
SECURITY_API_KEYS='[{"name":"First API key","value":"mySecret1"}],[{"name":"Second API key","value":"mySecret2"}]'

# Define one API key which expires in the future.
SECURITY_API_KEYS='[{"name":"My expiring API key","value":"mySecret!","expiresAt":"2100-01-01T00:00:00Z"}]'
```

#### `VALIDATOR_CHECK_FORMAT`

Checks whether the email address has a valid structure.
Please refer to [`py3-validate-email`][gitea-py3-validate-email] documentation
for further information.

```bash
# Enables email address structure check (default).
VALIDATOR_CHECK_FORMAT='true'

# Disables email address structure check.
VALIDATOR_CHECK_FORMAT='false'
```

#### `VALIDATOR_CHECK_BLACKLIST`

Checks the email address against a blacklist of domains.
Please refer to [`py3-validate-email`][gitea-py3-validate-email] documentation
for further information.

```bash
# Enables checking the email address against a blacklist of domains (default).
VALIDATOR_CHECK_BLACKLIST='true'

# Disables checking the email address against a blacklist of domains.
VALIDATOR_CHECK_BLACKLIST='false'
```

#### `VALIDATOR_CHECK_DNS`

Checks the DNS MX-records.
Please refer to [`py3-validate-email`][gitea-py3-validate-email] documentation
for further information.

```bash
# Enables checking the DNS MX-records (default).
VALIDATOR_CHECK_DNS='true'

# Disables checking the DNS MX-records.
VALIDATOR_CHECK_DNS='false'
```

#### `VALIDATOR_DNS_TIMEOUT`

Seconds until DNS timeout.
Please refer to [`py3-validate-email`][gitea-py3-validate-email] documentation
for further information.

```bash
# Wait 10 seconds until DNS timeout (default).
VALIDATOR_DNS_TIMEOUT='10'

# Wait 60 seconds until DNS timeout.
VALIDATOR_DNS_TIMEOUT='60'
```

#### `VALIDATOR_CHECK_SMTP`

Checks whether the email address actually exists by initiating an SMTP conversation.
Please refer to [`py3-validate-email`][gitea-py3-validate-email] documentation
for further information.

```bash
# Enables checking whether the email address actually exists
# by initiating an SMTP conversation (default).
VALIDATOR_CHECK_SMTP='true'

# Disables checking whether the email address actually exists
# by initiating an SMTP conversation.
VALIDATOR_CHECK_SMTP='false'
```

#### `VALIDATOR_SMTP_TIMEOUT`

Seconds until SMTP timeout.
Please refer to [`py3-validate-email`][gitea-py3-validate-email] documentation
for further information.

```bash
# Wait 10 seconds until SMTP timeout (default).
VALIDATOR_SMTP_TIMEOUT='10'

# Wait 60 seconds until SMTP timeout.
VALIDATOR_SMTP_TIMEOUT='60'
```

#### `VALIDATOR_SMTP_HELO_HOST`

Host name to use in SMTP HELO/EHLO. If not specified,
the fully qualified domain name of the local host is used.
Please refer to [`py3-validate-email`][gitea-py3-validate-email] documentation
for further information.

```bash
# Do not specify the host name to use in SMTP HELO/EHLO (default).
VALIDATOR_SMTP_HELO_HOST=''

# Specify the host name to use in SMTP HELO/EHLO.
VALIDATOR_SMTP_HELO_HOST='email-validator.example.com'
```

#### `VALIDATOR_SMTP_FROM_ADDRESS`

Email address used for the sender in the SMTP conversation.
If not specified, email addresses to be validated will be used as the senders as well.
Please refer to [`py3-validate-email`][gitea-py3-validate-email] documentation
for further information.

```bash
# Do not specify the email address used for the sender in the SMTP conversation (default).
VALIDATOR_SMTP_FROM_ADDRESS=''

# Specify the email address used for the sender in the SMTP conversation.
VALIDATOR_SMTP_FROM_ADDRESS='email-validator@example.com'
```

#### `VALIDATOR_SMTP_SKIP_TLS`

Skips the TLS negotiation with the server, even when available.

```bash
# Performs the TLS negotiation with the server (default).
VALIDATOR_SMTP_SKIP_TLS='false'

# Skips the TLS negotiation with the server.
VALIDATOR_SMTP_SKIP_TLS='true'
```

#### `VALIDATOR_SMTP_DEBUG`

Activates [`smtplib`][smtplib-docs]'s debug output which always goes to `stderr`.
Please refer to [`py3-validate-email`][gitea-py3-validate-email] documentation
for further information.

```bash
# Disables smtplib's debug output (default).
VALIDATOR_SMTP_DEBUG='false'

# Activates smtplib's debug output.
VALIDATOR_SMTP_DEBUG='true'
```

#### `VALIDATOR_ADDRESS_TYPES`

The IP address types to use. Useful when SMTP communication happens
through one interface only, but hosting server has a dual stack.
Please refer to [`py3-validate-email`][gitea-py3-validate-email] documentation
for further information.

```bash
# Allow IPv4 and IPv6 address types (default).
VALIDATOR_ADDRESS_TYPES='["IPv4", "IPv6"]'

# Allow IPv4 address type only.
VALIDATOR_ADDRESS_TYPES='["IPv4"]'
```

#### `HEALTH_CHECK_PING_WEBSITES`

A list of websites which should be pinged during the health check process.
Since email address verification requires an healthy internet connection
in order to talk with the DNS and the SMTP servers, by default example.com is "pinged".

Ping is not performed by sending an ICMP echo request because that protocol
is not supported on many infrastructures, such as Azure App Service.
Instead, an HTTP `HEAD` request is sent to specified websites.

Examples:

```bash
# Ping example.com (default).
HEALTH_CHECK_PING_WEBSITES='["https://example.com/"]'

# Do not ping any website.
HEALTH_CHECK_PING_WEBSITES='[]'
```

#### `HEALTH_CHECK_PING_TIMEOUT`

How many seconds should each website ping last before failing.
Timeout value should be greater than zero.

```bash
# Set ping timeout to 2 seconds (default).
HEALTH_CHECK_PING_TIMEOUT='2'

# Set ping timeout to 10 seconds.
HEALTH_CHECK_PING_TIMEOUT='10'
```

#### `APPINSIGHTS_CONNECTION_STRING`

If you want to send web service logs and metrics to Azure Application Insights,
then you can use this environment variable to set the connection string.

```bash
# Do not send logs and metrics to Application Insights (default).
APPINSIGHTS_CONNECTION_STRING=''

# Send logs and metrics to Application Insights, using the specified connection string.
APPINSIGHTS_CONNECTION_STRING='YOUR_CONNECTION_STRING'
```

#### `ZEROBOUNCE_API_KEY`

API key used to interact with ZeroBounce. Integration is performed during the
SMTP validation step, after all formal checks have been validated successfully.

An API key for ZeroBounce is **not** required, but it might be necessary
to perform [SMTP validation](#smtp-validation) when all required
infrastructural preconditions cannot be satisfied (e.g. on PaaS environments).

```bash
# Disable ZeroBounce integration (default).
ZEROBOUNCE_API_KEY=''

# Enable ZeroBounce integration, using the specified API key.
ZEROBOUNCE_API_KEY='YOUR_API_KEY'
```

#### `ZEROBOUNCE_CREDITS_THRESHOLD`

If ZeroBounce credits drop below given threshold,
then the web service health check will fail.
For example, if threshold have been set to 100,
health check will start failing at 99 credits remaining.

ZeroBounce credits are checked if and only if a ZeroBounce API key has been specified.

```bash
# Set ZeroBounce credits threshold to 1 (default).
ZEROBOUNCE_CREDITS_THRESHOLD='1'

# Set ZeroBounce credits threshold to 100.
ZEROBOUNCE_CREDITS_THRESHOLD='100'
```

### Logging

Web service writes log messages to the console.

If an Azure Application Insights connection string is specified using the
[`APPINSIGHTS_CONNECTION_STRING`](#appinsights_connection_string) configuration,
logs and telemetry data are also sent to that service.

### SMTP validation

After all formal checks have been validated successfully, then SMTP validation
is performed in order to understand whether the specified email address actually
has a working mailbox.

As specified in [`py3-validate-email` FAQs][gitea-py3-validate-email-faq],
SMTP validation has some infrastructural requirements which might be hard to satisfy.
For example, the service must be exposed on the internet with a static IP address
and a reverse DNS entry. Moreover, that static IP must not appear in any public blacklist.

Such conditions might be hard to satisfy on PaaS environments
and that is why this web service allows disabling SMTP validation
(see [`VALIDATOR_CHECK_SMTP`](#validator_check_smtp) configuration)
or it allows replacing it with an integration with ZeroBounce
(see [`ZEROBOUNCE_API_KEY`](#zerobounce_api_key) configuration).

Why not using ZeroBounce for all checks? This kind of integration with ZeroBounce
allows saving credits by eliminating most obvious mistakes,
such as email addresses with wrong formats or not existing domains.

## Usage

Please check OpenAPI docs on your instance (just visit the `/docs` URL).

For example, if an API key with value `my_key` has been set, the email address
"email@example.com" can be validated with the following request:

```http
GET /api/v1/validate/?address=email@example.com HTTP/1.1
Host: localhost:8080
X-Api-Key: my_key
```

### Verdicts

When an email address is validated, one of the following verdicts is responded:

- `valid`: all necessary elements so that an email message sent to that address
  is actually received are available.
- `risky`: the validation process could not be completed; therefore, there are
  not enough elements to confirm that the email address is valid or invalid.
- `invalid`: one of the necessary elements so that an email message sent to
  that address is actually received is not available.

`risky` verdict can be emitted for the following reasons:

| Reason code             | Description                                                                |
|-------------------------|----------------------------------------------------------------------------|
| `smtp_temporary_error`  | MX servers did not give a clear answer about the existence of the address. |
| `tls_negotiation_error` | An error has happened during the TLS negotiation.                          |

`invalid` verdict can be emitted for the following reasons:

| Reason code                | Description                                                                        |
|----------------------------|------------------------------------------------------------------------------------|
| `invalid_address_format`   | Email address had an invalid format.                                               |
| `domain_blacklisted`       | Domain of the email address was [blacklisted][github-disposable-email-domains].    |
| `domain_not_found`         | Domain was not found.                                                              |
| `no_nameserver_found`      | Domain did not resolve by nameservers in time.                                     |
| `dns_timeout`              | Domain lookup timed out.                                                           |
| `dns_configuration_error`  | DNS entries for this domain were falsely configured.                               |
| `no_mx_record`             | Domain had no MX records configured.                                               |
| `no_valid_mx_record`       | Domain had MX records configured, but none of them has a valid format.             |
| `address_not_deliverable`  | An MX server sent an SMTP reply which unambiguously indicate an invalid recipient. |
| `smtp_communication_error` | Communication with all MX servers was unsuccessful.                                |

Please refer to [`py3-validate-email`][gitea-py3-validate-email] documentation
and source code for further information.

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.

### Editing

[Visual Studio Code][vscode-website], with [Remote Containers extension][vscode-remote-containers],
is the recommended way to work on this project.

A development container has been configured with all required tools.

### Restoring dependencies

When starting the development container, dependencies should be automatically restored.

Anyway, this project uses [Poetry][poetry-website] to handle its dependencies:

```bash
poetry install
```

Poetry also manages the virtual environment, which can be activated with following command:

```bash
poetry shell
```

### Starting development server

A local development server listening on port `8080` can be started with following command:

```bash
poetry run start
```

### Running tests

Tests have been developed with [pytest][pytest-website] and can be run with following command:

```bash
poetry run test
```

Command above runs tests and also collects coverage information.

### Building Docker image

Docker image can be built with following command:

```bash
docker build . -f ./docker/Dockerfile -t $DOCKER_TAG
```

Please replace `$DOCKER_TAG` with a valid tag (e.g. `email-validator`).

## License

MIT © 2021-2022 [Alessio Parma][personal-website]

[docker-pulls-badge]: https://img.shields.io/docker/pulls/pommalabs/email-validator?style=flat-square
[docker-repository]: https://hub.docker.com/r/pommalabs/email-validator
[docker-stars-badge]: https://img.shields.io/docker/stars/pommalabs/email-validator?style=flat-square
[gitea-py3-validate-email]: https://gitea.ksol.io/karolyi/py3-validate-email
[gitea-py3-validate-email-faq]: https://gitea.ksol.io/karolyi/py3-validate-email/src/branch/master/FAQ.md
[github-disposable-email-domains]: https://github.com/disposable-email-domains/disposable-email-domains
[github-pymailcheck]: https://github.com/mailcheck/pymailcheck
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[github-starlite]: https://github.com/starlite-api/starlite
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/email-validator/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/email-validator/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[personal-website]: https://alessioparma.xyz/
[poetry-website]: https://python-poetry.org/
[project-license]: https://gitlab.com/pommalabs/email-validator/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[pytest-website]: https://docs.pytest.org/
[renovate-badge]: https://img.shields.io/badge/renovate-enabled-brightgreen.svg?style=flat-square
[renovate-website]: https://renovate.whitesourcesoftware.com/
[sendgrid-api]: https://docs.sendgrid.com/api-reference/e-mail-address-validation/validate-an-email
[sonar-coverage-badge]: https://img.shields.io/sonar/coverage/pommalabs_email-validator?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-quality-gate-badge]: https://img.shields.io/sonar/quality_gate/pommalabs_email-validator?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-website]: https://sonarcloud.io/dashboard?id=pommalabs_email-validator
[smtplib-docs]: https://docs.python.org/3/library/smtplib.html
[vscode-remote-containers]: https://code.visualstudio.com/docs/remote/containers
[vscode-website]: https://code.visualstudio.com/
[zerobounce-api]: https://www.zerobounce.net/docs/email-validation-api-quickstart#validate_emails__v2__
