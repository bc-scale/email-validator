FROM python:3.11-slim-bullseye AS base

ENV PROJECT_MODULE="email_validator"
ENV POETRY_VIRTUALENVS_IN_PROJECT="true"

# Prevents Python from writing pyc files to disc (equivalent to "python -B" option).
ENV PYTHONDONTWRITEBYTECODE=1

# Prevents Python from buffering stdout and stderr (equivalent to "python -u" option).
ENV PYTHONUNBUFFERED=1

# Skips the auto-update of built-in blacklist on startup.
# Blacklist is already downloaded during module installation.
ENV PY3VE_IGNORE_UPDATER=1

ENV PORT=8080
EXPOSE 8080

WORKDIR /opt/app
RUN useradd --uid 1000 app

# Poetry project files are required both by the builder and the runner.
# Builder needs them in order to restore dependencies.
# Runner needs them in order to execute commands using Poetry.
COPY ./pyproject.toml ./poetry.lock ./

FROM base AS builder

# Install build tools and curl.
RUN apt-get update -qq && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt-get install -y -qq --no-install-recommends build-essential curl && \
    rm -rf /var/lib/apt/lists/* /tmp/*

# Restore dependencies.
RUN export POETRY_HOME=/opt/poetry && \
    curl -sSL https://install.python-poetry.org | python3 - && \
    $POETRY_HOME/bin/poetry install --no-root --only main

FROM base AS runner

# Install dumb-init as process supervisor and init system.
RUN apt-get update -qq && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt-get install -y -qq --no-install-recommends dumb-init && \
    rm -rf /var/lib/apt/lists/* /tmp/*

# Copy restored dependencies.
COPY --from=builder /opt/app/.venv ./.venv

# Copy project files.
COPY ./scripts.py ./scripts.py
COPY ./src/${PROJECT_MODULE} ./${PROJECT_MODULE}

# Run as an unprivileged application user, whose UID is 1000. By specifying the numeric UID,
# "runAsNonRoot" flag within Kubernetes security context can be set to true.
USER 1000

# Set dumb-init as entrypoint, since uvicorn is not a process supervisor.
ENTRYPOINT ["dumb-init", "--"]

# Start uvicorn web server.
CMD .venv/bin/uvicorn --host 0.0.0.0 --port ${PORT} \
    --no-access-log --no-server-header ${PROJECT_MODULE}.main:app
