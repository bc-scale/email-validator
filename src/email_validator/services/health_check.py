# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import functools
import logging

import backoff
import httpx
from healthcheck import HealthCheck
from starlite.status_codes import HTTP_503_SERVICE_UNAVAILABLE
from zerobouncesdk import zerobouncesdk

from email_validator.settings import HealthCheckSettings, ZeroBounceSettings


@backoff.on_exception(backoff.expo, httpx.TransportError, max_tries=3)
def __ping_website(
    website: str,
    logger: logging.Logger,
    health_check_settings: HealthCheckSettings,
) -> tuple[bool, str]:
    """Returns True if specified website responds to an HTTP HEAD request."""
    response = httpx.head(
        website,
        timeout=health_check_settings.ping_timeout,
        follow_redirects=True,
    )

    if response.is_success:
        return (True, "Ping is OK")

    logger.warning(
        'Ping to "%s" responded %d. Response headers: %s',
        website,
        response.status_code,
        response.headers,
    )
    return (False, "Ping is KO")


def __check_zerobounce_credits(
    logger: logging.Logger,
    zerobounce_settings: ZeroBounceSettings,
) -> tuple[bool, str]:
    zerobounce_response = zerobouncesdk.get_credits()
    available_credits = int(zerobounce_response.credits)
    credits_threshold = zerobounce_settings.credits_threshold

    if available_credits >= credits_threshold:
        return True, "Credits check is OK"

    logger.warning(
        "Available ZeroBounce credits (%s) are below the configured threshold (%s)",
        available_credits,
        credits_threshold,
    )
    return False, f"Credits check is KO ({available_credits}/{credits_threshold})"


def get_health_check(
    logger: logging.Logger,
    health_check_settings: HealthCheckSettings,
    zerobounce_settings: ZeroBounceSettings,
) -> HealthCheck:
    health_check = HealthCheck(failed_status=HTTP_503_SERVICE_UNAVAILABLE)

    for i, ip_address in enumerate(health_check_settings.ping_websites, start=1):
        check = functools.partial(
            __ping_website, ip_address, logger, health_check_settings
        )
        setattr(check, "__name__", f"ping_website_{i}")
        health_check.add_check(check)

    if zerobounce_settings.api_key:
        check = functools.partial(
            __check_zerobounce_credits, logger, zerobounce_settings
        )
        setattr(check, "__name__", "check_zerobounce_credits")
        health_check.add_check(check)

    return health_check
