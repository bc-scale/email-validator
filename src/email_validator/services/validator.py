# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
from functools import lru_cache

import pymailcheck
import validate_email
from validate_email.email_address import AddressFormatError, EmailAddress
from validate_email.exceptions import (
    AddressNotDeliverableError,
    DNSConfigurationError,
    DNSTimeoutError,
    DomainBlacklistedError,
    DomainNotFoundError,
    EmailValidationError,
    NoMXError,
    NoNameserverError,
    NoValidMXError,
    SMTPCommunicationError,
    SMTPTemporaryError,
    TLSNegotiationError,
)
from zerobouncesdk import zerobouncesdk
from zerobouncesdk.zb_validate_status import ZBValidateStatus

from email_validator.models import (
    ValidationComponent,
    ValidationResponse,
    Verdict,
    VerdictReason,
)
from email_validator.settings import ValidatorSettings, ZeroBounceSettings

CUSTOM_PYMAILCHECK_DOMAINS = pymailcheck.DOMAINS.union(
    [
        # Additional Italian free email service providers:
        "alice.it",
        "libero.it",
        "tiscali.it",
    ]
)

VERDICT_RULES = {
    # Format related errors:
    AddressFormatError.__name__: (Verdict.INVALID, "invalid_address_format"),
    # Domain and DNS related errors:
    DomainBlacklistedError.__name__: (Verdict.INVALID, "domain_blacklisted"),
    DomainNotFoundError.__name__: (Verdict.INVALID, "domain_not_found"),
    NoNameserverError.__name__: (Verdict.INVALID, "no_nameserver_found"),
    DNSTimeoutError.__name__: (Verdict.INVALID, "dns_timeout"),
    DNSConfigurationError.__name__: (Verdict.INVALID, "dns_configuration_error"),
    NoMXError.__name__: (Verdict.INVALID, "no_mx_record"),
    NoValidMXError.__name__: (Verdict.INVALID, "no_valid_mx_record"),
    # SMTP related errors:
    AddressNotDeliverableError.__name__: (Verdict.INVALID, "address_not_deliverable"),
    SMTPCommunicationError.__name__: (Verdict.INVALID, "smtp_communication_error"),
    SMTPTemporaryError.__name__: (Verdict.RISKY, "smtp_temporary_error"),
    TLSNegotiationError.__name__: (Verdict.RISKY, "tls_negotiation_error"),
}

ZEROBOUNCE_STATUS_RULES = {
    ZBValidateStatus.valid: Verdict.VALID,
    ZBValidateStatus.invalid: Verdict.INVALID,
    ZBValidateStatus.catch_all: Verdict.VALID,
    ZBValidateStatus.unknown: Verdict.RISKY,
    ZBValidateStatus.spamtrap: Verdict.INVALID,
    ZBValidateStatus.abuse: Verdict.INVALID,
    ZBValidateStatus.do_not_mail: Verdict.INVALID,
}


class Validator:
    def __init__(
        self,
        logger: logging.Logger,
        validator_settings: ValidatorSettings,
        zerobounce_settings: ZeroBounceSettings,
    ):
        self.logger = logger
        self.validator_settings = validator_settings
        self.zerobounce_settings = zerobounce_settings

    @lru_cache
    def validate(self, address: str) -> ValidationResponse:
        address = address.lower()

        # Try to parse email address, so that we can expose account and domain information.
        address_parsed, user, domain = self.__parse_address(address)
        masked_address = self.__mask_address(user, domain)
        self.logger.info('Validating email address "%s"', masked_address)

        # Validation is performed in two steps: a cheap one and an expensive one.
        # Following "cheap" validation checks that email address is formally correct
        # and that domain is correctly configured (it does exist, it has MX records, ...).
        # If this check passes, then the "expensive" one is performed.
        validator, verdict, reason = self.__perform_formal_validation(address)

        # Validation is performed in two steps: a cheap one and an expensive one.
        # Following "expensive" validation checks that email address actually exists.
        # It relies on ZeroBounce, if API key has been specified, or it falls back
        # to "py3-validate-email" library, which talks to the SMTP servers of given email domain.
        if self.validator_settings.check_smtp and verdict is Verdict.VALID:
            validator, verdict, reason = self.__perform_smtp_validation(address)

        # If verdict is not valid and given address is formally correct,
        # then we can try to suggest the user the correct address
        # by checking it against common email providers.
        suggestion = None
        if verdict is not Verdict.VALID and address_parsed:
            suggestion = self.__suggest_alternative_address(address)

        self.logger.info(
            'Email address "%s" has been validated with verdict "%s",'
            ' reason code "%s" and suggestion "%s"',
            masked_address,
            verdict,
            reason.code if reason else "",
            self.__mask_suggestion(suggestion),
        )
        return ValidationResponse(
            address=address,
            user=user,
            domain=domain,
            validator=validator,
            verdict=verdict,
            reason=reason,
            suggestion=suggestion,
        )

    def __parse_address(self, address: str) -> tuple[bool, str | None, str | None]:
        try:
            parsed_address = EmailAddress(address)
            return True, parsed_address.user, parsed_address.domain
        except AddressFormatError:
            self.logger.warning("Email address could not be parsed", exc_info=True)
            return False, None, None

    def __mask_address(self, user: str | None, domain: str | None) -> str | None:
        if user and domain:
            mask = "*"
            if len(user) == 1:
                return f"{mask}@{domain}"
            return f"{user[0]}{mask*(len(user)-1)}@{domain}"
        return None

    def __mask_suggestion(self, suggestion: str | None) -> str | None:
        if not suggestion:
            return None
        parsed_suggestion = EmailAddress(suggestion)
        return self.__mask_address(parsed_suggestion.user, parsed_suggestion.domain)

    def __handle_validation_error(
        self, error: EmailValidationError
    ) -> tuple[Verdict, VerdictReason | None]:
        self.logger.warning(
            "An error was raised while validating email address. %s",
            error,
            exc_info=True,
        )
        error_type_name = type(error).__name__
        if error_type_name in VERDICT_RULES:
            verdict, reason_code = VERDICT_RULES[error_type_name]
            reason = VerdictReason(code=reason_code, message=str(error))
            return verdict, reason
        return Verdict.RISKY, None

    def __perform_formal_validation(
        self, address: str
    ) -> tuple[ValidationComponent, Verdict, VerdictReason | None]:
        try:
            is_valid = validate_email.validate_email_or_fail(
                address,
                check_format=self.validator_settings.check_format,
                check_blacklist=self.validator_settings.check_blacklist,
                check_dns=self.validator_settings.check_dns,
                dns_timeout=self.validator_settings.dns_timeout,
                check_smtp=False,  # SMTP check is performed in the next step.
                address_types=self.validator_settings.get_address_types(),
            )
            verdict = Verdict.VALID if is_valid else Verdict.RISKY
            reason = None
        except EmailValidationError as error:
            verdict, reason = self.__handle_validation_error(error)
        return ValidationComponent.PY3_VALIDATE_EMAIL, verdict, reason

    def __perform_smtp_validation(
        self, address: str
    ) -> tuple[ValidationComponent, Verdict, VerdictReason | None]:
        if self.zerobounce_settings.api_key:
            self.logger.info("Performing SMTP validation with ZeroBounce")
            return self.__perform_zerobounce_smtp_validation(address)
        self.logger.info("Performing SMTP validation with py3-validate-email")
        return self.__perform_local_smtp_validation(address)

    def __perform_local_smtp_validation(
        self, address: str
    ) -> tuple[ValidationComponent, Verdict, VerdictReason | None]:
        try:
            is_valid = validate_email.validate_email_or_fail(
                address,
                check_format=False,
                check_blacklist=False,
                check_dns=False,
                check_smtp=True,
                smtp_timeout=self.validator_settings.smtp_timeout,
                smtp_helo_host=self.validator_settings.smtp_helo_host,
                smtp_from_address=self.validator_settings.smtp_from_address,
                smtp_skip_tls=self.validator_settings.smtp_skip_tls,
                smtp_debug=self.validator_settings.smtp_debug,
                address_types=self.validator_settings.get_address_types(),
            )
            verdict = Verdict.VALID if is_valid else Verdict.RISKY
            reason = None
        except EmailValidationError as error:
            verdict, reason = self.__handle_validation_error(error)
        return ValidationComponent.PY3_VALIDATE_EMAIL, verdict, reason

    def __perform_zerobounce_smtp_validation(
        self, address: str
    ) -> tuple[ValidationComponent, Verdict, VerdictReason | None]:
        zerobounce_response = zerobouncesdk.validate(address)
        verdict = ZEROBOUNCE_STATUS_RULES[zerobounce_response.status]
        if verdict is not Verdict.VALID and zerobounce_response.sub_status:
            reason = VerdictReason(
                code=zerobounce_response.sub_status.name,
                message=zerobounce_response.error,
            )
        else:
            reason = None
        return ValidationComponent.ZEROBOUNCE, verdict, reason

    def __suggest_alternative_address(self, address: str) -> str | None:
        suggest_result = pymailcheck.suggest(
            address, domains=CUSTOM_PYMAILCHECK_DOMAINS
        )
        if suggest_result and suggest_result["full"] != address:
            return str(suggest_result["full"])
        return None
