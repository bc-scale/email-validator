# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from starlite_commons.services import get_logger
from zerobouncesdk import zerobouncesdk

from email_validator.settings import ZeroBounceSettings


def initialize_zerobounce_sdk(zerobounce_settings: ZeroBounceSettings) -> None:
    logger = get_logger()
    if zerobounce_settings.api_key:
        logger.info("ZeroBounce API key has been specified, SDK will be initialized")
        zerobouncesdk.initialize(zerobounce_settings.api_key)
