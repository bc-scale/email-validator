# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from pydantic_openapi_schema.v3_1_0 import Components
from starlite import (
    Controller,
    NotAuthorizedException,
    OpenAPIConfig,
    Parameter,
    Provide,
    Router,
    Starlite,
    get,
    post,
)
from starlite.status_codes import HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR
from starlite_commons import (
    API_KEY_SECURITY_SCHEME,
    API_KEY_SECURITY_SCHEME_NAME,
    APP_DESCRIPTION,
    APP_VERSION,
    MIT_LICENSE,
    ApiKeyAuthenticationMiddleware,
    HttpTracingMiddleware,
    check_health,
    get_version,
    redirect_to_docs,
)
from starlite_commons.services import (
    get_exporter,
    get_logger,
    get_logging_config,
    get_sampler,
    logging_exception_handler,
)
from starlite_commons.settings import AppInsightsSettings, SecuritySettings

from email_validator.models import ValidationRequest, ValidationResponse
from email_validator.services import Validator, get_health_check
from email_validator.services import (
    initialize_zerobounce_sdk as __initialize_zerobounce_sdk,
)
from email_validator.settings import (
    HealthCheckSettings,
    ValidatorSettings,
    ZeroBounceSettings,
)

root = Router(
    path="/",
    route_handlers=[redirect_to_docs, check_health, get_version],
    dependencies={
        "health_check_settings": Provide(HealthCheckSettings.load, use_cache=True),
        "health_check": Provide(get_health_check, use_cache=True),
    },
)


class EmailValidatorController(Controller):
    path = "/validate"
    tags = ["email_validator"]

    @get(
        status_code=HTTP_200_OK,
        raises=[NotAuthorizedException],
        summary="Validates specified email address.",
        description="The usage of the `POST` endpoint is recommended.",
        deprecated=True,
    )
    def get(
        self,
        validator: Validator,
        address: str = Parameter(
            description="The email address that is being validated."
        ),
    ) -> ValidationResponse:
        return validator.validate(address)

    @post(
        status_code=HTTP_200_OK,
        raises=[NotAuthorizedException],
        summary="Validates specified email address.",
    )
    def post(self, validator: Validator, data: ValidationRequest) -> ValidationResponse:
        return validator.validate(data.address)


security_settings = SecuritySettings.load()
appinsights_settings = AppInsightsSettings.load()
sampler = get_sampler(appinsights_settings)
exporter = get_exporter(appinsights_settings)

api_v1 = Router(
    path="/api/v1",
    route_handlers=[EmailValidatorController],
    security=[{API_KEY_SECURITY_SCHEME_NAME: []}],
    middleware=[
        ApiKeyAuthenticationMiddleware.define(get_logger, security_settings),
        HttpTracingMiddleware.define(sampler, exporter),
    ],
    exception_handlers={
        HTTP_500_INTERNAL_SERVER_ERROR: logging_exception_handler(get_logger)
    },
    dependencies={
        "validator": Provide(Validator),
        "validator_settings": Provide(ValidatorSettings.load, use_cache=True),
    },
)

zerobounce_settings = ZeroBounceSettings.load()


def initialize_zerobounce_sdk() -> None:
    __initialize_zerobounce_sdk(zerobounce_settings)


app = Starlite(
    openapi_config=OpenAPIConfig(
        title="Email Validator",
        description=APP_DESCRIPTION,
        version=APP_VERSION,
        license=MIT_LICENSE,
        components=Components(
            securitySchemes={API_KEY_SECURITY_SCHEME_NAME: API_KEY_SECURITY_SCHEME}
        ),
    ),
    route_handlers=[root, api_v1],
    logging_config=get_logging_config(appinsights_settings),
    dependencies={
        "logger": Provide(get_logger, use_cache=True),
        "zerobounce_settings": Provide(lambda: zerobounce_settings, use_cache=True),
    },
    on_startup=[initialize_zerobounce_sdk],
)
