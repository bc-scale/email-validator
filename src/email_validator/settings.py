# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from enum import Enum
from ipaddress import IPv4Address, IPv6Address
from typing import Any

from pydantic import BaseSettings, PrivateAttr
from pydantic.fields import Field
from validate_email.dns_check import AddressTypes


class AddressType(str, Enum):
    IP_V4 = "IPv4"
    IP_V6 = "IPv6"


class ValidatorSettings(BaseSettings):
    check_format: bool = Field(
        default=True,
        description="Checks whether the email address has a valid structure."
        " Default value is True.",
    )
    check_blacklist: bool = Field(
        default=True,
        description="Checks the email address against a blacklist of domains."
        " Default value is True.",
    )
    check_dns: bool = Field(
        default=True, description="Checks the DNS MX-records. Default value is True."
    )
    dns_timeout: int = Field(
        default=10,
        description="Seconds until DNS timeout. Default value is 10 seconds.",
        gt=0,
    )
    check_smtp: bool = Field(
        default=True,
        description="Checks whether the email address actually exists"
        " by initiating an SMTP conversation. Default value is True.",
    )
    smtp_timeout: int = Field(
        default=10,
        description="Seconds until SMTP timeout. Default value is 10 seconds.",
        gt=0,
    )
    smtp_helo_host: str | None = Field(
        default=None,
        description="Host name to use in SMTP HELO/EHLO. If set to None (the default),"
        " the fully qualified domain name of the local host is used.",
    )
    smtp_from_address: str | None = Field(
        default=None,
        description="Email address used for the sender in the SMTP conversation."
        " If set to None (the default), email addresses to be validated"
        " will be used as the senders as well.",
    )
    smtp_skip_tls: bool = Field(
        default=False,
        description="Skips the TLS negotiation with the server, even when available."
        " Default value is False.",
    )
    smtp_debug: bool = Field(
        default=False,
        description="Activates smtplib's debug output which always goes to stderr."
        " Default value is False.",
    )
    address_types: frozenset[AddressType] = Field(
        default=frozenset([AddressType.IP_V4, AddressType.IP_V6]),
        description="The IP address types to use. Default value is IPv4 and IPv6."
        " Useful when SMTP communication happens through one interface only,"
        " but hosting server has a dual stack.",
    )

    __address_types: AddressTypes = PrivateAttr()

    def __init__(self, **data: Any):
        super().__init__(**data)
        self.__init_address_types()

    class Config:
        env_prefix = "validator_"
        env_file = ".env"
        env_file_encoding = "utf-8"

    @staticmethod
    def load() -> "ValidatorSettings":
        return ValidatorSettings()

    def get_address_types(self) -> AddressTypes:
        return self.__address_types

    def __init_address_types(self) -> None:
        address_types: list[type[IPv4Address] | type[IPv6Address]] = []
        if AddressType.IP_V4 in self.address_types:
            address_types.append(IPv4Address)
        if AddressType.IP_V6 in self.address_types:
            address_types.append(IPv6Address)
        self.__address_types = frozenset(address_types)


class HealthCheckSettings(BaseSettings):
    ping_websites: tuple[str, ...] = Field(
        default=("https://example.com/",),
        description='Websites which should be "pinged" to ensure'
        " an healthy internet connection is available."
        " Ping is not performed by sending an ICMP echo request"
        " because that protocol is not supported on many infrastructures;"
        ' instead, an HTTP "HEAD" request is sent to specified websites.'
        " Default website is example.com (https://example.com/).",
    )

    ping_timeout: int = Field(
        default=2,
        description="How many seconds should each website ping last before failing."
        " Default value is 2 seconds, timeout value should be greater than zero.",
        gt=0,
    )

    class Config:
        env_prefix = "health_check_"
        env_file = ".env"
        env_file_encoding = "utf-8"

    @staticmethod
    def load() -> "HealthCheckSettings":
        return HealthCheckSettings()


class ZeroBounceSettings(BaseSettings):
    api_key: str | None = Field(
        default=None,
        description="API key used to interact with ZeroBounce.",
    )
    credits_threshold: int = Field(
        default=1,
        description="If ZeroBounce credits drop below given threshold,"
        " then the web service health check will fail.",
    )

    class Config:
        env_prefix = "zerobounce_"
        env_file = ".env"
        env_file_encoding = "utf-8"

    @staticmethod
    def load() -> "ZeroBounceSettings":
        return ZeroBounceSettings()
