# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from enum import Enum
from typing import Optional

from pydantic import BaseModel, Field


class ValidationRequest(BaseModel):
    """The request of the email address validation process."""

    address: str = Field(description="The email address that is being validated.")

    class Config:
        frozen = True


class ValidationComponent(str, Enum):
    """The component that produced the email address validation verdict."""

    PY3_VALIDATE_EMAIL = "py3_validate_email"
    ZEROBOUNCE = "zerobounce"


class Verdict(str, Enum):
    """
    Describes the three possible outcomes of the validation process.
    A valid verdict means that the email address is very likely to be existing.
    A risky verdict means that transient errors undermined the validation process.
    An invalid verdict means that the email address is very likely to be wrong or not existing.
    """

    VALID = "valid"
    RISKY = "risky"
    INVALID = "invalid"


class VerdictReason(BaseModel):
    """The details about why the email address was classified as risky or invalid."""

    code: str = Field(
        description="Error code."
        " Please check project README for further information."
    )
    message: Optional[str] = Field(description="Descriptive error message.")

    class Config:
        frozen = True


class ValidationResponse(BaseModel):
    """The response of the email address validation process."""

    address: str = Field(description="The email address that is being validated.")
    user: Optional[str] = Field(
        default=None,
        description='The portion of the email address before the "@" symbol.',
    )
    domain: Optional[str] = Field(
        default=None,
        description='The portion of the email address after the "@" symbol.',
    )
    validator: ValidationComponent = Field(
        description="The component that created the validation result."
        " It can assume the following values: py3_validate_email, zerobounce.",
    )
    verdict: Verdict = Field(
        description="A generic classification of whether or not the email address is valid."
        " It can assume the following values: valid, risky, invalid.",
    )
    reason: Optional[VerdictReason] = Field(
        default=None,
        description="When verdict is risky or invalid, the reason why the email address"
        " was marked as such is exposed as an error code and message.",
    )
    suggestion: Optional[str] = Field(
        default=None,
        description="A suggested correction in the event of domain name typos (e.g., gmial.com).",
    )

    class Config:
        frozen = True
