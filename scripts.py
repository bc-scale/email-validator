# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from starlite_commons.scripting import git_version as __git_version
from starlite_commons.scripting import start as __start
from starlite_commons.scripting import test as __test

PROJECT_MODULE = "email_validator"


def git_version():
    __git_version()


def start():
    __start(PROJECT_MODULE)


def test():
    __test(PROJECT_MODULE)
