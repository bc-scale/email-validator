# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
import pydoc
from typing import Type, cast
from unittest.mock import MagicMock, patch

import pymailcheck
import validate_email
from hypothesis import given
from hypothesis import strategies as st
from validate_email.email_address import EmailAddress
from validate_email.exceptions import DNSTimeoutError, EmailValidationError
from zerobouncesdk import zerobouncesdk
from zerobouncesdk.zb_validate_response import ZBValidateResponse
from zerobouncesdk.zb_validate_status import ZBValidateStatus
from zerobouncesdk.zb_validate_sub_status import ZBValidateSubStatus

from email_validator.models import ValidationComponent, Verdict
from email_validator.services.validator import (
    VERDICT_RULES,
    ZEROBOUNCE_STATUS_RULES,
    Validator,
)
from email_validator.settings import AddressType, ValidatorSettings, ZeroBounceSettings
from tests.utils import STUB_ZEROBOUNCE_API_KEY, email_address_strategy

DOMAINS_WITH_SUGGESTION = {
    "gmial.com": "gmail.com",
    "autlook.com": "outlook.com",
    "iahoo.com": "yahoo.com",
    "example.con": "example.com",
    "love.com": "live.com",
    "ticcali.it": "tiscali.it",
    "libero.itt": "libero.it",
}


def validator_factory(
    logger: logging.Logger = MagicMock(spec_set=logging.Logger),
    validator_settings: ValidatorSettings = ValidatorSettings(),
    zerobounce_settings: ZeroBounceSettings = ZeroBounceSettings(api_key=None),
):
    return Validator(
        logger=logger,
        validator_settings=validator_settings,
        zerobounce_settings=zerobounce_settings,
    )


################################################################################
# Address, account, domain
################################################################################


@given(email_address_strategy())
def test_that_validate_with_email_address_parses_address_user_domain(address: str):
    # Arrange
    validator = validator_factory()
    with patch.object(validate_email, "validate_email_or_fail", return_value=True):
        lower_address = address.lower()
        parsed_address = EmailAddress(lower_address)
        # Act
        result = validator.validate(address)
        # Assert
        assert result.address == lower_address
        assert result.user == parsed_address.user
        assert result.domain == parsed_address.domain


@given(st.text().filter(lambda t: "@" not in t))  # Avoids potential email addresses.
def test_that_validate_with_plain_text_does_not_parse_address_user_domain(
    plain_text: str,
):
    # Arrange
    validator = validator_factory()
    # Act
    result = validator.validate(plain_text)
    # Assert
    assert result.address == plain_text.lower()
    assert result.user is None
    assert result.domain is None


################################################################################
# Verdict (valid, risky, invalid)
################################################################################


@given(email_address_strategy())
def test_that_validate_with_valid_address_returns_valid_verdict(address: str):
    # Arrange
    validator = validator_factory()
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        result = validator.validate(address)
        # Assert
        assert mock.call_count == 2
        assert result.validator is ValidationComponent.PY3_VALIDATE_EMAIL
        assert result.verdict is Verdict.VALID
        assert result.reason is None


@given(email_address_strategy())
def test_that_validate_with_risky_address_returns_risky_verdict(address: str):
    # Arrange
    validator = validator_factory()
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=None
    ) as mock:
        # Act
        result = validator.validate(address)
        # Assert
        assert mock.call_count == 1
        assert result.validator is ValidationComponent.PY3_VALIDATE_EMAIL
        assert result.verdict is Verdict.RISKY
        assert result.reason is None


@given(
    email_address_strategy(),
    st.sampled_from(sorted(VERDICT_RULES.keys())),
)
def test_that_validate_with_invalid_address_returns_expected_verdict(
    address: str, verdict_rule_key: str
):
    # Arrange
    validator = validator_factory()
    verdict, reason_code = VERDICT_RULES[verdict_rule_key]
    error_type = cast(
        Type[EmailValidationError],
        pydoc.locate(f"validate_email.exceptions.{verdict_rule_key}"),
    )
    with patch.object(
        validate_email,
        "validate_email_or_fail",
        side_effect=error_type({}),
    ) as mock:
        # Act
        result = validator.validate(address)
        # Assert
        assert mock.call_count == 1
        assert result.validator is ValidationComponent.PY3_VALIDATE_EMAIL
        assert result.verdict is verdict
        assert result.reason is not None
        assert result.reason.code is reason_code


@given(
    email_address_strategy(),
    st.sampled_from(sorted(VERDICT_RULES.keys())),
)
def test_that_validate_with_smtp_error_returns_expected_verdict(
    address: str, verdict_rule_key: str
):
    # Arrange
    validator = validator_factory()
    verdict, reason_code = VERDICT_RULES[verdict_rule_key]
    error_type = cast(
        Type[EmailValidationError],
        pydoc.locate(f"validate_email.exceptions.{verdict_rule_key}"),
    )
    with patch.object(
        validate_email,
        "validate_email_or_fail",
        # First call will return True and it will interpreted as a valid address.
        # After that, SMTP validation will be performed and it will raise given error.
        side_effect=[True, error_type({})],
    ) as mock:
        # Act
        result = validator.validate(address)
        # Assert
        assert mock.call_count == 2
        assert result.validator is ValidationComponent.PY3_VALIDATE_EMAIL
        assert result.verdict is verdict
        assert result.reason is not None
        assert result.reason.code is reason_code


@given(email_address_strategy())
def test_that_validate_with_unhandled_validation_error_returns_risky_verdict(
    address: str,
):
    class UnhandledValidatorError(EmailValidationError):
        pass

    # Arrange
    validator = validator_factory()
    with patch.object(
        validate_email,
        "validate_email_or_fail",
        side_effect=UnhandledValidatorError(),
    ) as mock:
        # Act
        result = validator.validate(address)
        # Assert
        assert mock.call_count == 1
        assert result.validator is ValidationComponent.PY3_VALIDATE_EMAIL
        assert result.verdict is Verdict.RISKY
        assert result.reason is None


################################################################################
# ZeroBounce
################################################################################


@given(
    email_address_strategy(),
    st.sampled_from(ZBValidateStatus),
    st.sampled_from(ZBValidateSubStatus),
)
def test_that_validate_with_invalid_address_returns_expected_zerobounce_verdict(
    address: str,
    zerobounce_status_rule_key: ZBValidateStatus,
    zerobounce_sub_status: ZBValidateSubStatus,
):
    # Arrange
    zerobounce_settings = ZeroBounceSettings(api_key=STUB_ZEROBOUNCE_API_KEY)
    validator = validator_factory(zerobounce_settings=zerobounce_settings)
    verdict = ZEROBOUNCE_STATUS_RULES[zerobounce_status_rule_key]
    zerobounce_response = ZBValidateResponse(None)
    zerobounce_response.status = zerobounce_status_rule_key
    zerobounce_response.sub_status = zerobounce_sub_status
    with patch.object(
        validate_email,
        "validate_email_or_fail",
        return_value=True,
    ), patch.object(
        zerobouncesdk, "validate", return_value=zerobounce_response
    ) as mock:
        # Act
        result = validator.validate(address)
        # Assert
        assert mock.call_count == 1
        assert result.validator is ValidationComponent.ZEROBOUNCE
        assert result.verdict is verdict
        if verdict is Verdict.VALID:
            assert result.reason is None
        else:
            assert result.reason is not None
            assert result.reason.code == zerobounce_sub_status.name


################################################################################
# Suggestion
################################################################################


@given(email_address_strategy())
def test_that_validate_with_valid_address_returns_no_suggestion(address: str):
    # Arrange
    validator = validator_factory()
    with patch.object(validate_email, "validate_email_or_fail", return_value=True):
        # Act
        result = validator.validate(address)
        # Assert
        assert result.suggestion is None


@given(
    email_address_strategy(),
    st.sampled_from(sorted(DOMAINS_WITH_SUGGESTION.keys())),
)
def test_that_validate_with_risky_address_returns_suggestion(
    address: str, wrong_domain: str
):
    # Arrange
    validator = validator_factory()
    parsed_address = EmailAddress(address.lower())
    address = parsed_address.user + "@" + wrong_domain
    suggested_domain = DOMAINS_WITH_SUGGESTION[wrong_domain]
    with patch.object(validate_email, "validate_email_or_fail", return_value=None):
        # Act
        result = validator.validate(address)
        # Assert
        assert result.suggestion is not None
        parsed_suggestion = EmailAddress(result.suggestion)
        assert parsed_suggestion.user == parsed_address.user
        assert parsed_suggestion.domain == suggested_domain


@given(
    email_address_strategy(),
    st.sampled_from(sorted(DOMAINS_WITH_SUGGESTION.keys())),
)
def test_that_validate_with_invalid_address_returns_suggestion(
    address: str, wrong_domain: str
):
    # Arrange
    validator = validator_factory()
    parsed_address = EmailAddress(address.lower())
    address = parsed_address.user + "@" + wrong_domain
    suggested_domain = DOMAINS_WITH_SUGGESTION[wrong_domain]
    with patch.object(
        validate_email,
        "validate_email_or_fail",
        side_effect=DNSTimeoutError(),
    ):
        # Act
        result = validator.validate(address)
        # Assert
        assert result.suggestion is not None
        parsed_suggestion = EmailAddress(result.suggestion)
        assert parsed_suggestion.user == parsed_address.user
        assert parsed_suggestion.domain == suggested_domain


@given(st.text().filter(lambda t: "@" not in t))  # Avoids potential email addresses.
def test_that_validate_with_plain_text_returns_no_suggestion(plain_text: str):
    # Arrange
    validator = validator_factory()
    # Act
    result = validator.validate(plain_text)
    # Assert
    assert result.suggestion is None


@given(email_address_strategy())
def test_that_validate_with_suggestion_equal_to_address_returns_no_suggestion(
    address: str,
):
    # Arrange
    validator = validator_factory()
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=None
    ), patch.object(
        pymailcheck, "suggest", return_value={"full": address.lower()}
    ) as mock:
        # Act
        result = validator.validate(address)
        # Assert
        mock.assert_called_once()
        assert result.suggestion is None


################################################################################
# Masking
################################################################################


@given(email_address_strategy())
def test_that_validate_logs_masked_address(address: str):
    # Arrange
    logger = MagicMock(spec_set=logging.Logger)
    validator = validator_factory(logger=logger)
    with patch.object(validate_email, "validate_email_or_fail", return_value=None):
        # Act
        result = validator.validate(address)
        # Assert
        logger.info.assert_called()
        args = [arg for call in logger.info.call_args_list for arg in call.args]
        assert result.address not in args


@given(
    email_address_strategy(),
    st.sampled_from(sorted(DOMAINS_WITH_SUGGESTION.keys())),
)
def test_that_validate_logs_masked_suggestion(address: str, wrong_domain: str):
    # Arrange
    logger = MagicMock(spec_set=logging.Logger)
    validator = validator_factory(logger=logger)
    parsed_address = EmailAddress(address.lower())
    address = parsed_address.user + "@" + wrong_domain
    with patch.object(validate_email, "validate_email_or_fail", return_value=None):
        # Act
        result = validator.validate(address)
        # Assert
        logger.info.assert_called()
        args = [arg for call in logger.info.call_args_list for arg in call.args]
        assert result.suggestion not in args


################################################################################
# Settings
################################################################################


@given(email_address_strategy(), st.booleans())
def test_that_validate_uses_check_format_setting(address: str, check_format: bool):
    # Arrange
    validator_settings = ValidatorSettings(check_format=check_format)
    validator = validator_factory(validator_settings=validator_settings)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=False
    ) as mock:
        # Act
        validator.validate(address)
        # Assert
        assert "check_format" in mock.call_args.kwargs
        assert mock.call_args.kwargs["check_format"] == check_format


@given(email_address_strategy(), st.booleans())
def test_that_validate_uses_check_blacklist_setting(
    address: str, check_blacklist: bool
):
    # Arrange
    validator_settings = ValidatorSettings(check_blacklist=check_blacklist)
    validator = validator_factory(validator_settings=validator_settings)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=False
    ) as mock:
        # Act
        validator.validate(address)
        # Assert
        assert "check_blacklist" in mock.call_args.kwargs
        assert mock.call_args.kwargs["check_blacklist"] == check_blacklist


@given(email_address_strategy(), st.booleans())
def test_that_validate_uses_check_dns_setting(address: str, check_dns: bool):
    # Arrange
    validator_settings = ValidatorSettings(check_dns=check_dns)
    validator = validator_factory(validator_settings=validator_settings)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=False
    ) as mock:
        # Act
        validator.validate(address)
        # Assert
        assert "check_dns" in mock.call_args.kwargs
        assert mock.call_args.kwargs["check_dns"] == check_dns


@given(email_address_strategy(), st.integers().filter(lambda t: t > 0))
def test_that_validate_uses_dns_timeout_setting(address: str, dns_timeout: int):
    # Arrange
    validator_settings = ValidatorSettings(dns_timeout=dns_timeout)
    validator = validator_factory(validator_settings=validator_settings)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=False
    ) as mock:
        # Act
        validator.validate(address)
        # Assert
        assert "dns_timeout" in mock.call_args.kwargs
        assert mock.call_args.kwargs["dns_timeout"] == dns_timeout


@given(email_address_strategy(), st.booleans())
def test_that_validate_uses_check_smtp_setting(address: str, check_smtp: bool):
    # Arrange
    validator_settings = ValidatorSettings(check_smtp=check_smtp)
    validator = validator_factory(validator_settings=validator_settings)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(address)
        # Assert
        expected_call_count = 2 if check_smtp else 1
        assert mock.call_count == expected_call_count
        first_call_args = mock.call_args_list[0]
        assert "check_smtp" in first_call_args.kwargs
        assert first_call_args.kwargs["check_smtp"] is False
        if check_smtp:
            second_call_args = mock.call_args_list[1]
            assert "check_smtp" in second_call_args.kwargs
            assert second_call_args.kwargs["check_smtp"] is True


@given(email_address_strategy(), st.integers().filter(lambda t: t > 0))
def test_that_validate_uses_smtp_timeout_setting(address: str, smtp_timeout: int):
    # Arrange
    validator_settings = ValidatorSettings(smtp_timeout=smtp_timeout)
    validator = validator_factory(validator_settings=validator_settings)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(address)
        # Assert
        assert "smtp_timeout" in mock.call_args.kwargs
        assert mock.call_args.kwargs["smtp_timeout"] == smtp_timeout


@given(email_address_strategy(), st.text())
def test_that_validate_uses_smtp_helo_host_setting(address: str, smtp_helo_host: str):
    # Arrange
    validator_settings = ValidatorSettings(smtp_helo_host=smtp_helo_host)
    validator = validator_factory(validator_settings=validator_settings)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(address)
        # Assert
        assert "smtp_helo_host" in mock.call_args.kwargs
        assert mock.call_args.kwargs["smtp_helo_host"] == smtp_helo_host


@given(email_address_strategy(), st.text())
def test_that_validate_uses_smtp_from_address_setting(
    address: str, smtp_from_address: str
):
    # Arrange
    validator_settings = ValidatorSettings(smtp_from_address=smtp_from_address)
    validator = validator_factory(validator_settings=validator_settings)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(address)
        # Assert
        assert "smtp_from_address" in mock.call_args.kwargs
        assert mock.call_args.kwargs["smtp_from_address"] == smtp_from_address


@given(email_address_strategy(), st.booleans())
def test_that_validate_uses_smtp_skip_tls_setting(address: str, smtp_skip_tls: bool):
    # Arrange
    validator_settings = ValidatorSettings(smtp_skip_tls=smtp_skip_tls)
    validator = validator_factory(validator_settings=validator_settings)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(address)
        # Assert
        assert "smtp_skip_tls" in mock.call_args.kwargs
        assert mock.call_args.kwargs["smtp_skip_tls"] == smtp_skip_tls


@given(email_address_strategy(), st.booleans())
def test_that_validate_uses_smtp_debug_setting(address: str, smtp_debug: bool):
    # Arrange
    validator_settings = ValidatorSettings(smtp_debug=smtp_debug)
    validator = validator_factory(validator_settings=validator_settings)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(address)
        # Assert
        assert "smtp_debug" in mock.call_args.kwargs
        assert mock.call_args.kwargs["smtp_debug"] == smtp_debug


@given(email_address_strategy(), st.frozensets(st.sampled_from(AddressType)))
def test_that_validate_uses_address_type_setting(
    address: str, address_types: frozenset[AddressType]
):
    # Arrange
    validator_settings = ValidatorSettings(address_types=address_types)
    validator = validator_factory(validator_settings=validator_settings)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(address)
        # Assert
        assert mock.call_count == 2
        for call in mock.mock_calls:
            assert "address_types" in call.kwargs
            assert (
                call.kwargs["address_types"] == validator_settings.get_address_types()
            )
