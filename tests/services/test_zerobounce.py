# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.


import logging
from unittest.mock import patch

from zerobouncesdk import zerobouncesdk

from email_validator.services import initialize_zerobounce_sdk
from email_validator.settings import ZeroBounceSettings
from tests.utils import STUB_ZEROBOUNCE_API_KEY


def test_that_initializer_does_not_log_about_zerobounce_availability_when_not_available():
    # Arrange
    zerobounce_settings = ZeroBounceSettings(api_key=None)
    with patch.object(logging.Logger, "info") as mock:
        # Act
        initialize_zerobounce_sdk(zerobounce_settings)
        # Assert
        mock.assert_not_called()


def test_that_initializer_does_not_initialize_zerobounce_when_not_available():
    # Arrange
    zerobounce_settings = ZeroBounceSettings(api_key=None)
    with patch.object(logging.Logger, "info"), patch.object(
        zerobouncesdk, "initialize"
    ) as mock:
        # Act
        initialize_zerobounce_sdk(zerobounce_settings)
        # Assert
        mock.assert_not_called()


def test_that_initializer_initializes_zerobounce_when_available():
    # Arrange
    zerobounce_settings = ZeroBounceSettings(api_key=STUB_ZEROBOUNCE_API_KEY)
    with patch.object(zerobouncesdk, "initialize") as mock:
        # Act
        initialize_zerobounce_sdk(zerobounce_settings)
        # Assert
        mock.assert_called_once()
