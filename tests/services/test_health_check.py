# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
from ipaddress import IPv4Address, IPv6Address
from unittest.mock import MagicMock, patch

import httpx
from healthcheck import HealthCheck
from hypothesis import given
from hypothesis import strategies as st
from starlite.status_codes import (
    HTTP_200_OK,
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_503_SERVICE_UNAVAILABLE,
)
from zerobouncesdk import zerobouncesdk
from zerobouncesdk.zb_get_credits_response import ZBGetCreditsResponse

from email_validator.services import get_health_check
from email_validator.settings import HealthCheckSettings, ZeroBounceSettings
from tests.utils import STUB_ZEROBOUNCE_API_KEY


def health_check_factory(
    logger: logging.Logger = MagicMock(spec_set=logging.Logger),
    health_check_settings: HealthCheckSettings = HealthCheckSettings(),
    zerobounce_settings: ZeroBounceSettings = ZeroBounceSettings(api_key=None),
):
    return get_health_check(
        logger=logger,
        health_check_settings=health_check_settings,
        zerobounce_settings=zerobounce_settings,
    )


################################################################################
# Ping website
################################################################################


@given(st.ip_addresses())
def test_that_health_check_returns_200_when_ping_website_check_succeeds(
    website: IPv4Address | IPv6Address,
):
    # Arrange
    health_check_settings = HealthCheckSettings(ping_websites=(str(website),))
    health_check = health_check_factory(health_check_settings=health_check_settings)
    ok_response = MagicMock()
    ok_response.is_success = True
    ok_response.status_code = HTTP_200_OK
    with patch.object(
        httpx,
        "head",
        return_value=ok_response,
    ):
        # Act
        (_, status_code, _) = health_check.run()
        # Assert
        assert status_code == HTTP_200_OK


@given(st.ip_addresses())
def test_that_health_check_returns_503_when_ping_website_check_fails(
    website: IPv4Address | IPv6Address,
):
    # Arrange
    health_check_settings = HealthCheckSettings(ping_websites=(str(website),))
    health_check = health_check_factory(health_check_settings=health_check_settings)
    ko_response = MagicMock()
    ko_response.is_success = False
    ko_response.status_code = HTTP_500_INTERNAL_SERVER_ERROR
    with patch.object(
        httpx,
        "head",
        return_value=ko_response,
    ):
        # Act
        (_, status_code, _) = health_check.run()
        # Assert
        assert status_code == HTTP_503_SERVICE_UNAVAILABLE


################################################################################
# ZeroBounce
################################################################################

ZEROBOUNCE_CREDITS_THRESHOLD = 100


def test_that_health_check_runs_zerobounce_credits_check_when_zerobounce_is_available():
    # Arrange
    health_check_settings = HealthCheckSettings(ping_websites=())
    zerobounce_settings = ZeroBounceSettings(api_key=STUB_ZEROBOUNCE_API_KEY)
    with patch.object(HealthCheck, "add_check") as mock:
        # Act
        health_check_factory(
            health_check_settings=health_check_settings,
            zerobounce_settings=zerobounce_settings,
        )
        # Assert
        mock.assert_called_once()


def test_that_health_check_does_not_run_zerobounce_credits_check_when_zerobounce_is_not_available():
    # Arrange
    health_check_settings = HealthCheckSettings(ping_websites=())
    zerobounce_settings = ZeroBounceSettings(api_key=None)
    with patch.object(HealthCheck, "add_check") as mock:
        # Act
        health_check_factory(
            health_check_settings=health_check_settings,
            zerobounce_settings=zerobounce_settings,
        )
        # Assert
        mock.assert_not_called()


@given(st.integers().filter(lambda i: i >= ZEROBOUNCE_CREDITS_THRESHOLD))
def test_that_health_check_returns_200_when_zerobounce_credits_check_succeeds(
    available_credits: int,
):
    # Arrange
    health_check_settings = HealthCheckSettings(ping_websites=())
    zerobounce_settings = ZeroBounceSettings(
        api_key=STUB_ZEROBOUNCE_API_KEY, credits_threshold=ZEROBOUNCE_CREDITS_THRESHOLD
    )
    health_check = health_check_factory(
        health_check_settings=health_check_settings,
        zerobounce_settings=zerobounce_settings,
    )
    zerobounce_response = ZBGetCreditsResponse(f'{{"Credits":{available_credits}}}')
    with patch.object(zerobouncesdk, "get_credits", return_value=zerobounce_response):
        # Act
        (_, status_code, _) = health_check.run()
        # Assert
        assert status_code == HTTP_200_OK


@given(st.integers().filter(lambda i: i < ZEROBOUNCE_CREDITS_THRESHOLD))
def test_that_health_check_returns_503_when_zerobounce_credits_check_fails(
    available_credits: int,
):
    # Arrange
    health_check_settings = HealthCheckSettings(ping_websites=())
    zerobounce_settings = ZeroBounceSettings(
        api_key=STUB_ZEROBOUNCE_API_KEY, credits_threshold=ZEROBOUNCE_CREDITS_THRESHOLD
    )
    health_check = health_check_factory(
        health_check_settings=health_check_settings,
        zerobounce_settings=zerobounce_settings,
    )
    zerobounce_response = ZBGetCreditsResponse(f'{{"Credits":{available_credits}}}')
    with patch.object(zerobouncesdk, "get_credits", return_value=zerobounce_response):
        # Act
        (_, status_code, _) = health_check.run()
        # Assert
        assert status_code == HTTP_503_SERVICE_UNAVAILABLE
