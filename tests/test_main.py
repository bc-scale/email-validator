# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from typing import cast
from unittest.mock import Mock

from hypothesis import given
from starlite import DefineMiddleware, Provide, TestClient, create_test_client
from starlite.status_codes import HTTP_200_OK
from starlite_commons import ApiKeyAuthenticationMiddleware

from email_validator.main import (
    EmailValidatorController,
    api_v1,
    app,
    initialize_zerobounce_sdk,
)
from email_validator.models import ValidationComponent, ValidationResponse, Verdict
from email_validator.services import Validator
from tests.utils import email_address_strategy

################################################################################
# Configuration
################################################################################


def test_that_app_can_be_loaded():
    # Act
    with TestClient(app=app) as client:
        # Assert
        assert client


def test_that_api_v1_router_enforces_authentication():
    # Assert
    assert any(
        filter(
            lambda m: cast(DefineMiddleware, m).middleware
            is ApiKeyAuthenticationMiddleware,
            api_v1.middleware,
        )
    )


def test_that_app_has_expected_on_startup_handlers():
    # Assert
    assert len(app.on_startup) == 1
    assert initialize_zerobounce_sdk in app.on_startup


################################################################################
# Validation
################################################################################

VALIDATION_ENDPOINT = "/validate"


@given(email_address_strategy())
def test_that_validation_with_post_invokes_validator(address: str):
    # Arrange
    validator = Mock(spec_set=Validator)
    validator.validate.return_value = ValidationResponse(
        address=address,
        validator=ValidationComponent.PY3_VALIDATE_EMAIL,
        verdict=Verdict.VALID,
    )
    with create_test_client(
        route_handlers=EmailValidatorController,
        dependencies={"validator": Provide(lambda: validator)},
    ) as client:
        # Act
        response = client.post(VALIDATION_ENDPOINT, json={"address": address})
        # Assert
        validator.validate.assert_called_once_with(address)
        assert response.status_code == HTTP_200_OK


@given(email_address_strategy())
def test_that_validation_with_get_invokes_validator(address: str):
    # Arrange
    validator = Mock(spec_set=Validator)
    validator.validate.return_value = ValidationResponse(
        address=address,
        validator=ValidationComponent.PY3_VALIDATE_EMAIL,
        verdict=Verdict.VALID,
    )
    with create_test_client(
        route_handlers=EmailValidatorController,
        dependencies={"validator": Provide(lambda: validator)},
    ) as client:
        # Act
        response = client.get(VALIDATION_ENDPOINT, params={"address": address})
        # Assert
        validator.validate.assert_called_once_with(address)
        assert response.status_code == HTTP_200_OK
