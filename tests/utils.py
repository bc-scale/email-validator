# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from hypothesis import strategies as st
from starlite_commons.settings import ApiKey
from validate_email.email_address import EmailAddress
from validate_email.exceptions import AddressFormatError

STUB_LOCAL_API_KEY = ApiKey(name="TEST", value="API_KEY")
STUB_ZEROBOUNCE_API_KEY = "API_KEY"


def __is_parsable_email_address(address: str) -> bool:
    try:
        EmailAddress(address)
        return True
    except AddressFormatError:
        return False


def email_address_strategy():
    return st.emails().filter(__is_parsable_email_address)
